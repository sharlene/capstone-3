import React, { Fragment } from 'react';
import Banner from '../components/Banner';
import Slider from '../components/Slider';
import AdminDashboard from '../adminPages/AdminDashboard';

export default function Home() {
  const user = {
    isAdmin: false 
  };

  return (
    <Fragment>
      {!user.isAdmin ? (
        <Fragment>
          <Banner />
          <Slider />
        </Fragment>
      ) : (
        <AdminDashboard />
      )}
    </Fragment>
  );
}
