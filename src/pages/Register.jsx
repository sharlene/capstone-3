import {Form, Button} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {Navigate, useNavigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register(){
	const navigate = useNavigate()
	const {user} = useContext(UserContext)
	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNo, setMobileNo] = useState('')

	// determines if the button is disabled or not
	const [isActive, setIsActive] = useState(false)

	useEffect(() => {
		((email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== '' && mobileNo.length === 11) && (password1 === password2)) ?
		setIsActive(true):setIsActive(false)
	}, [firstName, lastName, mobileNo, email, password1, password2])


  function registerUser(event) {
    event.preventDefault();
  
    fetch(`${process.env.REACT_APP_API_URL}/users/check-email`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then(response => response.json())
      .then(result => {
        if(result === true){
          Swal.fire({
            title: 'Oops!',
            icon: 'error',
            text: 'Email is already in use!',
          });
        } else {
          return fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
			  mobileNo: mobileNo,
              password: password1,
            }),
          })
            .then(response => response.json())
            .then(result => {
              if(typeof result !== "undefined"){
                // for clearing form
                setEmail('');
                setPassword1('');
                setPassword2('');
                setFirstName('');
                setLastName('');
                setMobileNo('');
  
                Swal.fire({
                  title: 'Successful!',
                  icon: 'success',
                  text: 'User successfully registered!',
                })

                navigate("/login")
              }
            })
            .catch(error => {
              Swal.fire({
                title: 'Error!',
                icon: 'error',
                text: 'Something went wrong!',
              });
            });
        }
      });
  }
  
  

	return (
		(user.id == null) ?

			<Form onSubmit={event => registerUser(event)}>
				<Form.Group controlId="firstName">
	                <Form.Label>First name</Form.Label>
	                <Form.Control 
		                type="text" 
		                placeholder="Enter first name"
		                value={firstName}
		                onChange={event => setFirstName(event.target.value)}
		                required
	                />	              
	            </Form.Group>

	            <Form.Group controlId="lastName">
	                <Form.Label>Last name</Form.Label>
	                <Form.Control 
		                type="text" 
		                placeholder="Enter last name"
		                value={lastName}
		                onChange={event => setLastName(event.target.value)}
		                required
	                />	              
	            </Form.Group>

	            <Form.Group controlId="mobileNo">
	                <Form.Label>Mobile no</Form.Label>
	                <Form.Control 
		                type="text" 
		                placeholder="Enter mobile no"
		                value={mobileNo}
		                onChange={event => setMobileNo(event.target.value)}
		                required
	                />	              
	            </Form.Group>

	            <Form.Group controlId="userEmail">
	                <Form.Label>Email address</Form.Label>
	                <Form.Control 
		                type="email" 
		                placeholder="Enter email"
		                value={email}
		                onChange={event => setEmail(event.target.value)}
		                required
	                />
	                <Form.Text className="text-muted">
	                    We'll never share your email with anyone else.
	                </Form.Text>
	            </Form.Group>

	            <Form.Group controlId="password1">
	                <Form.Label>Password</Form.Label>
	                <Form.Control 
		                type="password" 
		                placeholder="Password" 
		                value={password1}
		                onChange={event => setPassword1(event.target.value)}
		                required
	                />
	            </Form.Group>

	            <Form.Group controlId="password2">
	                <Form.Label>Verify Password</Form.Label>
	                <Form.Control 
		                type="password" 
		                placeholder="Verify Password"
		                value={password2}
		                onChange={event => setPassword2(event.target.value)}
		                required
	                />
	            </Form.Group>

	            {	isActive ? 
	            	<Button variant="primary" type="submit" id="submitBtn">
	            		Submit
	            	</Button>
	            	:
	            	<Button variant="primary" type="submit" id="submitBtn" disabled>
	            		Submit
	            	</Button>
	            }

	        </Form>
	    :
	    	<Navigate to="/shop"/>
	)
}