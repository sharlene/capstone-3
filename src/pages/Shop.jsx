import React, { useState, useEffect } from 'react';
import { Container, Row } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';

export default function Shop() {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        const fetchProducts = async () => {
            const res = await fetch(`${process.env.REACT_APP_API_URL}/products`);
            const data = await res.json();
            setProducts(data);
        }
        fetchProducts();
    }, []);

    return (
        <Container>
            <Row className="d-flex justify-content-around">
                {products.map(product => (
                    <ProductCard key={product.slug} product={product} />
                ))}
            </Row>
        </Container>
    );
}
