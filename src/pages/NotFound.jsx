import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import '../App.css';

const pages = [
  { path: '/', content: 'Home page content', name: 'Home' },
  // add all pages here...
];

export default function NotFound() {
  const [search, setSearch] = useState('');
  const navigate = useNavigate();
  
  const handleSearchChange = (event) => {
    setSearch(event.target.value);
  }

  const handleSearchSubmit = (event) => {
    event.preventDefault();
    const results = searchPages(search);
    // TODO: Display the results to the user
    console.log(results); // For now, just log the results to the console
    navigate(`/search?query=${search}`);
  }

  function searchPages(query) {
    return pages.filter(page => page.content.toLowerCase().includes(query.toLowerCase()));
  }

  return (
    <div className="notFound">
      <h1>404: Page Not Found</h1>
      <p>The page you are looking for could not be found.</p>
      <form onSubmit={handleSearchSubmit}>
        <input type="text" placeholder="Search..." value={search} onChange={handleSearchChange} />
        <button type="submit">Search</button>
      </form>
      <Link to="" onClick={() => navigate(-1)}>Go back to previous page</Link>
    </div>
  );
}

