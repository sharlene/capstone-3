import { useContext, useEffect, useState } from 'react'
import {Row, Col, Form, Button} from 'react-bootstrap'
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import swal from 'sweetalert';

export default function Login(){
    const { user, setUser } = useContext(UserContext)
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isActive, setIsActive] = useState(false)

    function loginUser(event){
        event.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/api/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type':'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(response => response.json())
        .then(result => {
            if(typeof result.accessToken !== 'undefined'){
                localStorage.setItem('token', result.accessToken)
                retrieveUserDetails(result.accessToken)
               
                setEmail('')
                setPassword('')
        
                swal.fire({
                    title: "User successfully logged in!",
                    icon: "success",
                    text: "User successfully logged in!"})
            } 
        }).catch(error => {
            swal.fire({
                title: "Authentication failed!",
                icon: "error",
                text: "Kindly check your login credentials"
            })
        })

        // refresh the page
        // window.location.reload();
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/api/users/me`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(response => response.json())
        .then(result => {
            setUser({
                id: result._id,
                isAdmin: result.isAdmin
            })
        })
    }

    useEffect(()=> {
        if(email !=='' && password !=='') {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])

    return(
        (user.id !== null) ?
            <Navigate to="/courses"/>
        :    
            <Row className="justify-content-md-center align-items-center">
            <Col xs={12} md={4} className='custom-form'>
                <Form className='my-5' onSubmit={(event)=> loginUser(event)}>
                <h1 className='text-center my-2'>Unleash The Charm!</h1>
                    <Form.Group controlId="userEmail" className='p-2'>
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email" 
                            value={email}
                            onChange={event => setEmail(event.target.value)}
                            required
                            autoComplete='email'
                        />
                        <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                            type="password" 
                            placeholder="Password" 
                            value={password}
                            onChange={event => setPassword(event.target.value)}
                            required
                            autoComplete='current-password'
                        />
                    </Form.Group>
                    {/* conditional rendering is when you render elements depending on a specific logical condition */}
                    {
                        isActive ?
                        <Button variant="primary" type="submit" id="submitBtn" className='my-3'>
                        Submit
                        </Button>
                        :
                        <Button disabled variant="primary" type="submit" id="submitBtn" className='my-3'>
                        Submit
                        </Button>
                    }
                    <p className='my-3'>Not yet registered? <Link to="/register">Register here.</Link></p>
                </Form>
                </Col>
            </Row>
    )
}
