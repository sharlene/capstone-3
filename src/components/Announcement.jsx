import styled from "styled-components"


const Container = styled.div`
    background-color: #d85757;
    color: white;
`


export default function Announcement() {
    return (
        <Container className="text-center container-fluid">
            Super Launch Sale! Free Shipping on Orders Over PHP 2000.00!
        </Container>
    )
}