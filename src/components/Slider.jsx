import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';


const slides = [
    { id: 1, imageUrl: '/img/slider-dog-1.png' },
    { id: 2, imageUrl: '/img/slider-dog-2.png' },
    { id: 3, imageUrl: '/img/slider-dog-3.png' },
    // Add more slides as needed
  ];
  

export default function homeSlider(){
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
      }
    return (
        
        <Slider {...settings}>
            {slides.map(slide => (
                <div key={slide.id}>
                <img src={slide.imageUrl} alt={`Slide ${slide.id}`} />
                </div>
            ))}
            </Slider>
  );
}
