import { useState, useEffect } from 'react';
import { Container, Row } from 'react-bootstrap';
import ProductCard from './ProductCard';

export default function Category({ match }) {
    const [products, setProducts] = useState([]);
    const category = match.params.category;

    useEffect(() => {
        const fetchProducts = async () => {
            const res = await fetch(`/api/products/${category}`);
            const data = await res.json();
            setProducts(data);
        }
        fetchProducts();
    }, [category]);

    return (
        <Container>
            <Row className="d-flex justify-content-around">
                {products.map(product => (
                    <ProductCard key={product.slug} product={product} />
                ))}
            </Row>
        </Container>
    )
}
