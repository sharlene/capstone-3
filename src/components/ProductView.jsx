import React, { useState, useEffect, useContext } from 'react';
import { useParams, useNavigate, Link } from 'react-router-dom';
import { Container, Card, Button, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import LoadingSpinner from '../components/Spinner';

export default function ProductView() {
  const navigate = useNavigate();
  const { user } = useContext(UserContext);
  const { productId } = useParams();

  const [product, setProduct] = useState({ colors: [], sizes: [] });
  const [selectedColor, setSelectedColor] = useState("");
  const [selectedSize, setSelectedSize] = useState("");
  const [quantity, setQuantity] = useState(1);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
    .then(response => response.json())
    .then(data => {
      setProduct(data);
      setIsLoading(false);
    })
  }, [productId]);

  const addToCart = (e) => {
    e.preventDefault();
    const cartItem = {
      productId: product._id,
      productName: product.name,
      quantity: quantity, 
      price: product.price,
      size: selectedSize,
      color: selectedColor
    };

    fetch(`${process.env.REACT_APP_API_URL}/cart`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(cartItem)
    })
    .then(response => response.json())
    .then(result => {
      if(result.error){
        Swal.fire("Error", result.error, "error");
      } else {
        Swal.fire("Success", "Product added to cart.", "success");
        navigate("/cart");
      }
    }).catch(error => {
      Swal.fire("Oopsie daisy", "Something went wrong :(", "error");
    })
  }

  return (
    <Container className="mt-5">
      <Col lg={{ span: 6, offset: 3 }}>
        <Card className="productCard p-3">
          <Card.Body>
            {isLoading ? 
              <LoadingSpinner /> 
              : 
              <>
                <Card.Title>{product.name}</Card.Title>
                <Card.Img variant="top" src={product.image} />
                <Card.Subtitle className="mt-3">Price:</Card.Subtitle>
                <Card.Text>${product.price}</Card.Text>

                <form onSubmit={addToCart}>
                  <h4>Colors:</h4>
                  <select value={selectedColor} onChange={(e) => setSelectedColor(e.target.value)}>
                    {product.colors.map(color => (
                      <option key={color} value={color}>
                        {color}
                      </option>
                    ))}
                  </select>

                  <h4>Sizes:</h4>
                  <select value={selectedSize} onChange={(e) => setSelectedSize(e.target.value)}>
                    {product.sizes.map(size => (
                      <option key={size} value={size}>
                        {size}
                      </option>
                    ))}
                  </select>

                  <h4>Quantity:</h4>
                  <input type="number" value={quantity} onChange={(e) => setQuantity(e.target.value)} />

                  {user.id !==null ?
                    <Button type="submit">Add to Cart</Button>
                  :
                    <Link className="btn btn-warning" to="/login"> 
                      Log In to Order
                    </Link>
                  }
                </form>
              </>
            }
          </Card.Body>
        </Card>
      </Col>
    </Container>
  )
}
