import {Row, Col} from 'react-bootstrap'

export default function Banner(){
    return (
        <Row>
            <Col className='justify-content-md-center'>
            <img
              alt=""
              src="/img/banner-name.svg" width="100%"
            />{' '}
            </Col>
        </Row>
    )
}
