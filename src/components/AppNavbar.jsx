import { Fragment, useContext } from 'react'
import {Container, Navbar, Nav, Button, Form, NavDropdown} from 'react-bootstrap'
import { Link } from 'react-router-dom'

import UserContext from '../UserContext'


export default function AppNavbar() {
    const { user } = useContext(UserContext)

  return (
    <Navbar bg="light" expand="lg" className='sticky-top'>
      <Container fluid>
        <Navbar.Brand as={Link} to="/">
            <img
              alt="logo"
              src="/img/logo.png"
              width="30"
              height="30"
              className="d-inline-block align-top"
            />{' '}
            Posh Pups</Navbar.Brand>
        
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            navbarScroll
          >
            
          { (user.isAdmin === true) ?

                <Fragment>
                  {/* admin navbar */}
                  <Nav.Link as={Link} to="/dashboard">Dashboard</Nav.Link>
                  <Nav.Link as={Link} to="/blog">Blog</Nav.Link>
                  <Nav.Link as={Link} to="/orders">Orders</Nav.Link>
                  <Nav.Link as={Link} to="/products">Products</Nav.Link>
                  <Nav.Link as={Link} to="/users">Users</Nav.Link>
                </Fragment>
                :
                <Fragment>
                {/* universal user */}

                <NavDropdown title="Shop" id="navbarScrollingDropdown" >
                  <NavDropdown.Item href="/shop">All</NavDropdown.Item>
                  <NavDropdown.Item href="/formalwear">Fromal Wear</NavDropdown.Item>
                  <NavDropdown.Item href="/hoodies">Hoodies</NavDropdown.Item>
                  <NavDropdown.Item href="/casualewar">Casual Wear</NavDropdown.Item>
                  <NavDropdown.Item href="/sleepwear">Sleep Wear</NavDropdown.Item>
                  <NavDropdown.Item href="/costumes">Costumes</NavDropdown.Item>
                  <NavDropdown.Item href="/accessories">Accessories</NavDropdown.Item>
        
                  <NavDropdown.Divider />

                  <NavDropdown.Item href="/custom">Custom Made</NavDropdown.Item>
                  <NavDropdown.Item href="/:category">Promotions and Discounts</NavDropdown.Item>

                </NavDropdown>

                <Nav.Link href="/about">About</Nav.Link>
                <Nav.Link href="/blog">Blog</Nav.Link>
                <Nav.Link href="/contact">Contact Us</Nav.Link>
            

                { (user.id !== null) ?
                    <Fragment>
                      <Nav.Link as={Link} to="/account">Account</Nav.Link>
                      <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                    </Fragment>
                    :
                    <Fragment>
                      <Nav.Link as={Link} to="/login">Login</Nav.Link>
                      <Nav.Link as={Link} to="/register">Register</Nav.Link>
                    </Fragment>
                  }
              
                    </Fragment>
            }
          </Nav>
          <Form className="d-flex">
            <Form.Control
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
            />
            <Button variant="outline-success">Search</Button>
          </Form>
          
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
