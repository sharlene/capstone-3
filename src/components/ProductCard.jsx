import { Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import propTypes from 'prop-types';

export default function ProductCard({ product }) {
    const { slug, name, price, image } = product;
   
    return(
        <Col xs={12} md={4}>
            <Card className='productCard p-3'>
                <Card.Img variant="top" src={image} />
                <Card.Body>
                    <Card.Title>{name}</Card.Title>

                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>{price}</Card.Text>
                    <Link className="btn btn-primary" to={`/products/${slug}`}>View Product</Link>
                </Card.Body>
            </Card>
        </Col>
    )
}

ProductCard.propTypes = {
    product: propTypes.shape({
        name: propTypes.string.isRequired,
        price: propTypes.number.isRequired,
        image: propTypes.string.isRequired,
    })
}
