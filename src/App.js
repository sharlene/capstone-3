import React, { useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Container } from 'react-bootstrap';
import { UserProvider } from "./UserContext";

import AppNavbar from './components/AppNavbar';

import Home from './pages/Home';
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./components/Logout";
import Announcement from "./components/Announcement";
import Category from "./components/Category";
import NotFound from "./pages/NotFound";
import ProductView from "./components/ProductView";
import Shop from "./pages/Shop";

export default function App() {
    const [user, setUser] = useState({
        id: null,
        isAdmin: null
    })

    const unsetUser = () => {
        localStorage.clear()
    }
    return (
        <UserProvider value ={{user, setUser, unsetUser}}>
        <Router>
        <Announcement />
        <AppNavbar/>
            <Container>
                <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/register" element={<Register />} />
                <Route path="/login" element={<Login />} />
                <Route path="/logout" element={<Logout/>}/>
                <Route path="/category/:category" element={<Category/>}/>
                <Route path="/products/:slug" element={<ProductView />}/>
                <Route path="/shop" element={<Shop />}/>
                <Route path="*" element={<NotFound/>}/>
                </Routes>
            </Container>
        </Router>
        </UserProvider>
    );
}
